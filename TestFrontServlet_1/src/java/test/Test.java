/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import annotation.UrlAnnotation;
import java.sql.Date;
import model.ModelView;

/**
 *
 * @author karen
 */
public class Test {
    private int nbr;
    private Date dt;

    public Date getDt() {
        return dt;
    }

    public void setDt(Date dt) {
        this.dt = dt;
    }
    
    public int getNbr() {
        return nbr;
    }

    public void setNbr(int nbr) {
        this.nbr = nbr;
    }
    
    @UrlAnnotation(url = "tester")
    public ModelView get(){
        ModelView mv=new ModelView();
        mv.setAttribute("liste","listee");
        mv.setAttribute("name", dt);
        mv.setUrl("/index.jsp");
        return mv;
    }
}
