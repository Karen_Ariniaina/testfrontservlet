<%-- 
    Document   : index
    Created on : 5 nov. 2022, 18:51:42
    Author     : karen
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World! <%= request.getAttribute("liste") %></h1>
        <h1>Hello World! <%= request.getAttribute("name") %></h1>
    </body>
</html>
